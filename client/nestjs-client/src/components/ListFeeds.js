import Feed from "./Feed";
import React, { useState, useEffect } from 'react';

export default function ListFeeds(params) {

  const [items, setItems] = useState(null);
  const [gItems, setGItems] = useState('Loading...');

  useEffect(() => {
    document.title = 'HN Feed'
    getItems()
  }, [])

  const getItems = async() => {
    const urlGet = 'http://localhost:3000/feeds/list'
    await fetch(urlGet,{method: 'POST'})
    .then(api => api.json())
    .then(datos => {
      const existe = Array.from(datos).find(element => Number(element.userDrop) === 0)
      if(existe){
        setItems(datos)
      }else {
        setGItems('no data available')
      }
    })
  }

  

  return (
    <div className="feed-list">
    {
      (items && items.length > 0) ?
      items.map(feed => {
        if(!feed.userDrop && (feed.title !== null || feed.storyTitle != null))
        {
          return <Feed key={feed.objectID} data={feed} />
        }
        else{
          return null
        }
        
      }) 
      : <div>{gItems}</div>
    }
    </div>
  );
}
