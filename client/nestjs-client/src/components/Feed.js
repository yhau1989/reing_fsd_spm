import "../App.css";
import {useState} from 'react'

export default function Feed(params) {
  const [view, setView] = useState(true);

  const {
    createdAt,
    title,
    url,
    author,
    objectID,
    storyTitle,
    storyUrl,
  } = params.data;

  const parseDate = (dateAt) => {
    const dateNow = new Date();
    const dateItem = new Date(dateAt);
    const rtf = new Intl.RelativeTimeFormat("en", { numeric: "auto" });
    let dayUTCNow = dateNow.getDate();
    let dateUTCItem = dateItem.getDate();
    let inDay = "";
    if (dateUTCItem - dayUTCNow === -1) {
      let s = rtf.format(dateUTCItem - dayUTCNow, "day");
      inDay = s.charAt(0).toUpperCase() + s.slice(1);
    } else if (dateUTCItem - dayUTCNow === 0) {
      let amPm = dateItem.getHours() <= 10 ? "am" : "pm";
      inDay = `${dateItem.getHours()}:${dateItem.getMinutes()} ${amPm}`;
    } else if (dateUTCItem - dayUTCNow < -1) {
      let humanFotmat = dateItem.toDateString().split(" ");
      inDay = `${humanFotmat[1]} ${humanFotmat[2]}`;
    }

    return inDay;
  };

  const setValid = (hurl, url) => {
    return hurl ? hurl : url;
  };

  const trahs = async(id) => {
    const urlGet = `http://localhost:3000/feeds/remove/${id}`
    const api = await fetch(urlGet,{method: 'POST'})
    let rsl = await api.json()
    console.log('remove', rsl);
    setView(!view)
  };

  if(view)
  {
    return <div key={objectID} className="feed">
      <a
        className="feed-details"
        href={setValid(storyUrl, url)}
        target="_blank"
        rel="noopener noreferrer"
      >
        <div className="feed-tittle">{setValid(storyTitle, title)}</div>
        <div className="author">{`- ${author} -`}</div>
      </a>
      <div className="feed-controls">
        <div className="create_at">{parseDate(createdAt)}</div>
        <div className="trash" onClick={() => trahs(objectID)}>
          <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M17 5V4C17 2.89543 16.1046 2 15 2H9C7.89543 2 7 2.89543 7 4V5H4C3.44772 5 3 5.44772 3 6C3 6.55228 3.44772 7 4 7H5V18C5 19.6569 6.34315 21 8 21H16C17.6569 21 19 19.6569 19 18V7H20C20.5523 7 21 6.55228 21 6C21 5.44772 20.5523 5 20 5H17ZM15 4H9V5H15V4ZM17 7H7V18C7 18.5523 7.44772 19 8 19H16C16.5523 19 17 18.5523 17 18V7Z"
              fill="currentColor"
            />
            <path d="M9 9H11V17H9V9Z" fill="currentColor" />
            <path d="M13 9H15V17H13V9Z" fill="currentColor" />
          </svg>
        </div>
      </div>
    </div>
  }
  else
  {
    return null
  }
  
  
}
