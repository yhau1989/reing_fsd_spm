const mongoose = require("mongoose");
mongoose.connect(
  "mongodb://root:example@mongo-server_mongo_1:27017/test?authSource=admin",
  { useNewUrlParser: true, useUnifiedTopology:true }
);

const { Schema } = mongoose;
const feedsSchema = new Schema({
  createdAt: String,
  title: String,
  url: String,
  author: String,
  objectID: String,
  storyTitle: String,
  storyUrl: String,
  userDrop: Boolean,
});

const Feed = mongoose.model("Feeds", feedsSchema);

function testConnections(){
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
      console.log("open connection..!");
  });
};

async function findByObjectID(id: string): Promise<any>{
  let result = {};
  await Feed.findOne({ objectID: id }, (err: any, feed: any) => {
    result =  (err) ? err : feed;
  });
  return result;
};

const newFeet = async(feedobject: {}): Promise<any> => {
    let result: any = 0;
  try {
    const feed = new Feed(feedobject);
    result = await feed.save();
  } catch (error) {
    result = error;
  }
  return result;
};

module.exports = { newFeet, findByObjectID, testConnections };
