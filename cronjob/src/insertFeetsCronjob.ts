export {}
let fetch = require("node-fetch");
let ocr = require("./ocr2");

async function getFeeds(): Promise<any>{
 let items = {};
  const url_api_algolia =
    "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
  try {
    let api = await fetch(url_api_algolia);
    let data = await api.json();
    if (data.hits && data.hits.length > 0) {
      items = {'status': 0, 'data': data.hits};
    }
  } catch (error) {
    items = {'status': 99, 'error': error};
  }
  return items;
};

async function process(): Promise<void> {

  try {
    let dataApi : any = await getFeeds();
    if(dataApi?.status == 0){
       dataApi?.data?.map(async(item: any) => {
        ocr.findByObjectID(item.objectID).then((exist) => {
          if(exist == null)
          {
            ocr.newFeet({
                  createdAt: item.created_at,
                  title:  item.title,
                  url: item.url,
                  author: item.author,
                  objectID: item.objectID,
                  storyTitle : item.story_title,
                  storyUrl: item.story_url,
                  userDrop: false,
                }).then(saved => {
                  console.log('inserted', saved?._id);
                });
          }
        });
      });
    }
    else{
      console.log('process error', dataApi.error);
    }
  } catch (error) {
    console.log('process error general', error);
  }
}


process();