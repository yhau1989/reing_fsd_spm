const fetch = require("node-fetch");
const ocr = require("./ocr");

const getFeeds = async () => {
 let items = null;
  const url_api_algolia =
    "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
  try {
    let api = await fetch(url_api_algolia);
    let data = await api.json();
    if (data.hits && data.hits.length > 0) {
      items = {'status': 0, 'data': data.hits};
    }
  } catch (error) {
    items = {'status': 99, 'error': error};
  }
  return items;
};

const process = async() => {
  let dataApi = await getFeeds();
  let exist = null;
  let saved = null;

  try {
    if(dataApi.status == 0){
      dataApi.data.map(async(item) => {
        exist = await ocr.findByObjectID(item.objectID);
        if(exist == null)
        {
          saved = await ocr.newFeet({
            createdAt: item.created_at,
            title:  item.title,
            url: item.url,
            author: item.author,
            objectID: item.objectID,
            storyTitle : item.story_title,
            storyUrl: item.story_url,
            userDrop: false,
          });
          console.log('inserted', saved._id);
        }
      });
      console.log('process end');
    }
    else{
      console.log('process error', dataApi.error);
    }
  } catch (error) {
    console.log('process error general', error);
  }
  
}


const getLista = async() => {
  let g = await ocr.getList();
  console.log(g);
}


function testConnections(){
  ocr.testConnections();
}

const updateOne = async() => {
  let h = await ocr.desabledFeed('25480857')
  console.log(h)
}


updateOne();
//process();
//testConnections();