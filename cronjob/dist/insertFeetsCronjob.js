"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
let fetch = require("node-fetch");
let ocr = require("./ocr2");
function getFeeds() {
    return __awaiter(this, void 0, void 0, function* () {
        let items = {};
        const url_api_algolia = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
        try {
            let api = yield fetch(url_api_algolia);
            let data = yield api.json();
            if (data.hits && data.hits.length > 0) {
                items = { 'status': 0, 'data': data.hits };
            }
        }
        catch (error) {
            items = { 'status': 99, 'error': error };
        }
        return items;
    });
}
;
function process() {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let dataApi = yield getFeeds();
            if ((dataApi === null || dataApi === void 0 ? void 0 : dataApi.status) == 0) {
                (_a = dataApi === null || dataApi === void 0 ? void 0 : dataApi.data) === null || _a === void 0 ? void 0 : _a.map((item) => __awaiter(this, void 0, void 0, function* () {
                    ocr.findByObjectID(item.objectID).then((exist) => {
                        if (exist == null) {
                            ocr.newFeet({
                                createdAt: item.created_at,
                                title: item.title,
                                url: item.url,
                                author: item.author,
                                objectID: item.objectID,
                                storyTitle: item.story_title,
                                storyUrl: item.story_url,
                                userDrop: false,
                            }).then(saved => {
                                console.log('inserted', saved === null || saved === void 0 ? void 0 : saved._id);
                            });
                        }
                    });
                }));
            }
            else {
                console.log('process error', dataApi.error);
            }
        }
        catch (error) {
            console.log('process error general', error);
        }
    });
}
process();
//# sourceMappingURL=insertFeetsCronjob.js.map