var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const mongoose = require("mongoose");
mongoose.connect("mongodb://root:example@mongo-server_mongo_1:27017/test?authSource=admin", { useNewUrlParser: true, useUnifiedTopology: true });
const { Schema } = mongoose;
const feedsSchema = new Schema({
    createdAt: String,
    title: String,
    url: String,
    author: String,
    objectID: String,
    storyTitle: String,
    storyUrl: String,
    userDrop: Boolean,
});
const Feed = mongoose.model("Feeds", feedsSchema);
function testConnections() {
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error:"));
    db.once("open", function () {
        console.log("open connection..!");
    });
}
;
function findByObjectID(id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = {};
        yield Feed.findOne({ objectID: id }, (err, feed) => {
            result = (err) ? err : feed;
        });
        return result;
    });
}
;
const newFeet = (feedobject) => __awaiter(this, void 0, void 0, function* () {
    let result = 0;
    try {
        const feed = new Feed(feedobject);
        result = yield feed.save();
    }
    catch (error) {
        result = error;
    }
    return result;
});
module.exports = { newFeet, findByObjectID, testConnections };
//# sourceMappingURL=ocr2.js.map