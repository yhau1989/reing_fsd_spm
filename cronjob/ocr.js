const mongoose = require("mongoose");
mongoose.connect(
  "mongodb://root:example@127.0.0.1:8081/test?authSource=admin",
  { useNewUrlParser: true, useUnifiedTopology:true }
);

const { Schema } = mongoose;
const feedsSchema = new Schema({
  createdAt: String,
  title: String,
  url: String,
  author: String,
  objectID: String,
  storyTitle: String,
  storyUrl: String,
  userDrop: Boolean,
});

const Feed = mongoose.model("Feeds", feedsSchema);

function testConnections(){
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
      console.log("open connection..!");
  });
};

async function getList(){
  let result = 0;
  await Feed.find({}, (err, feed) => {
    result =  (err) ? handleError(err) : feed;
  });
  return result;
};


async function findByObjectID(id){
  let result = 0;
  await Feed.findOne({ objectID: id }, (err, feed) => {
    result =  (err) ? handleError(err) : feed;
  });
  return result;
};

const newFeet = async(feedobject) => {
    let result = 0;
  try {
    let feed = new Feed(feedobject);
    result = await feed.save();
  } catch (error) {
    result = handleError(error);
  }
  return result;
};


const desabledFeed = async(id) => {
  let result = 0;
  try {
    result = await Feed.updateOne({ objectID: id }, { userDrop: true });
  } catch (error) {
    result = error;
  }
  return result;

}


module.exports = { newFeet, findByObjectID, testConnections, getList, desabledFeed};
