# Reing_FSD_SPM

Full Stack Developer CHALLENGE by Samuel Pilay Muñoz


## Create a docker network
```docker
docker network create reing-network
```

## Create 4 containers

### Mongo container
```linux
cd /mongo-server
docker-compose.yml up
```

### CronJob container
```linux
cd /cronjob
Docker build -t node-cron-job .
docker run -it -p 8082:8082 --name docker-node-cronjob -d --network reing-network --link mongo-server_mongo_1 node-cron-job
```

### NestApi container
```linux
cd /api
Docker build -t docker-node-nestjs .
docker run -it -p 3000:3000 --name nestJs-api -d --network reing-network --link mongo-server_mongo_1 docker-node-nestjs 
```

### React Client container
```linux
cd /client/nestjs-client
Docker build -t node-react-app .
docker run -it -p 3006:3006 --name react-app-feed -d --network reing-network node-react-app
```

### Problem with CORE in chrome ?? 
[Install CORS Unblock extension](https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino)
